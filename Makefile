SAL_DIR = /tmp/multipart-form-data-salmonella/


default: module


module: multipart-form-data.scm
	csc -s -O3 -j multipart-form-data multipart-form-data.scm


test: module tests/run.scm
	chicken-install -test -sudo

salmonella:
	rm -rf $(SAL_DIR)
	mkdir -p $(SAL_DIR)
	salmonella --log-file=$(SAL_DIR)log
	salmonella-html-report $(SAL_DIR)log $(SAL_DIR)html
	firefox $(SAL_DIR)html/index.html

clean:
	rm -f *.o *.so multipart-form-data.import.scm
	rm -f tests/*.so tests/*.o tests/*.import.scm tests/run
