;;; ===========================================================================
;;; RFC 2388 multipart/form-data
;;; ===========================================================================
;;; Parses a multipart/form-data mime-part into a field name, a filename and
;;; content-type for uploads, and a port that yields the value of the field.  The
;;; port is backed by a string for "small" field values and a file for "big" field
;;; values; "small" and "big" can be configured.
;;;
;;; multipart/form-data is a MIME content-type used to transfer form data that
;;; is too complex to send as url-encoded values.  The MIME message will have a
;;; multipart structure, as specified in RFC 1521 Section 7.2, consisting of a
;;; sequence of mime-parts which are each an RFC 822 MIME message.  See the
;;; multipart egg for iterating over these mime parts.
;;;
;;; This egg parses a particular multipart subtype: multipart/form-data.  Each
;;; mime-part in a multipart/form-data message should adhere to RFC 2388.  In
;;; particular:
;;;
;;; - Content-disposition should have a values of form-data and have an
;;;   additional property 'name' which is the unique name of a form field.  An
;;;   optional 'filename' property may be present if the form field represents
;;;   a file payload.
;;; - Content-type should be present if the form field represents a file
;;;   payload.
;;;
;;; Assumptions:
;;; - 'name' and 'filename' properties of the content-disposition header are
;;;   encoded in ASCII.  No RFC 1522 encoding.
;;; - Makes no attempt to use the optional content-transfer-encoding header to
;;;   decode field values if they were encoded.  Content-transfer-encoding
;;;   isn't usually used in HTTP form-data messages, but may be used in mail
;;;   systems.  For example, SMTP requires 7-bit characters and restricts line
;;;   lengths to 1000, so encoding field values may be necessary.
;;; - mime-parts with a content-type of multipart/mixed are not supported and
;;;   the nested mime-parts will not be descended into.  multipart/mixed is
;;;   depreciated in HTML 5, and no browsers support it as of this writing.
;;;
;;;
;;; Copyright (C) 2013, Bryan Vicknair
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are met:
;;; 
;;; Redistributions of source code must retain the above copyright notice, this
;;; list of conditions and the following disclaimer.  Redistributions in binary
;;; form must reproduce the above copyright notice, this list of conditions and
;;; the following disclaimer in the documentation and/or other materials
;;; provided with the distribution.  Neither the name of the author nor the
;;; names of its contributors may be used to endorse or promote products
;;; derived from this software without specific prior written permission. 
;;; 
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.



(module multipart-form-data
  (form-data-max-mem form-data-max-file
   make-form-datum
   form-datum-name
   form-datum-filename
   form-datum-ctype
   form-datum-port
   form-datum-tempfile
   mime-part->form-datum
   cdispo=form-data?
   cdispo-name
   cdispo-filename
   parse-headers
   make-stream-repeater-input-port)

(import scheme chicken ports extras data-structures)
(use rfc822 srfi-1 srfi-13 posix files)


; TODO: Test performance of large file against python CGI or other.

; TODO: where to put high-level function (with...) that gathers all fields into
; alist and cleans up tempfiles 

; TODO: How to name parameters so they don't conflict w/ parameters in other
; modules?



; The largest amount of data to store in an in-memory buffer for a single
; form-data field.  Fields values that are larger than this will be written to
; disk.
(define form-data-max-mem (make-parameter 4096))

; The largest allowable form-data field value.  Field values larger than this
; will raise an exception when encountered.
(define form-data-max-file (make-parameter (let ((mb (expt 2 20))) (* 20 mb))))



;;; A mime-part that has a content-disposition of form-data can be parsed into
;;; a record of this type.
;;;
;;; filename and ctype are only non-false for file uploads, and even they are
;;; optional.
;;;
;;; port is an input-port that will yield the value of the field.  It may be
;;; backed by an in-memory buffer, or a temporary file, depending on the
;;; form-data-max-mem parameter in this module.
;;;
;;; tempfile is the name of the file used to store the fields value if its size
;;; was larger than the form-data-max-mem parameter, or false otherwise.
(define-record form-datum name filename ctype port tempfile)


;;; Parse a mime-part into a form-datum, or #f if the mime-part headers
;;; are not RFC 2388 compliant.
;;;
;;; port should be a port that yields the contents of a single mime-part in a
;;; multipart mime message, who's content-disposition is form-data.
;;;
;;; Note: Does not delete tempfile created for form-datum.
(define (mime-part->form-datum port)
  (let* ((fname (create-temporary-file))
         (headers (parse-headers port))
         )
    (define-values (backing-port fname) (make-stream-repeater-input-port port
                                                                         (form-data-max-mem)
                                                                         (form-data-max-file)))
    (if headers
      (make-form-datum  (car headers)
                        (cadr headers)
                        (caddr headers)
                        backing-port
                        fname)
      #f)))






;;; ===========================================================================
;;; Header Parsing
;;; ===========================================================================

;;; Is the given Content-disposition field-body of type 'form-data'?  That is
;;; the only type that we understand in this module.
(define (cdispo=form-data? cdispo-field-body)
  (let ((tokens (rfc822-field->tokens cdispo-field-body)))
    (and (not (null? tokens)) (string=? (car tokens) "form-data"))))


;;; Get the value of an 'attribute' in the field body of a Content-disposition
;;; header.  An attribute is a key="value" pair, like name or filename.
;;;
;;; #f if the attribute name can't be found.
(define (cdispo-attr-value cdispo-field-body attr)
  (let* ((tokens (rfc822-field->tokens cdispo-field-body))
         (attr-and-rest (find-tail (lambda (s)
                                     (and (string? s)
                                          (string=? s (conc attr "="))))
                                    tokens)))
      (and attr-and-rest
           (> (length attr-and-rest) 1)
           (cadr attr-and-rest))))


;;; Get the 'name' attribute from a Content-disposition header field.
(define (cdispo-name cdispo-field-body)
  (cdispo-attr-value cdispo-field-body 'name))


;;; Get the 'filename attribute from a Content-disposition header field.
(define (cdispo-filename cdispo-field-body)
  (cdispo-attr-value cdispo-field-body 'filename))


; Parses form-data mime-part headers into a list:
;
;   (field-name filename content-type)
;
; or #f if the content-disposition of the headers is not
; form-data.  filename and content-type may be #f if not found.
(define (parse-headers port)
  (let* ((headers (rfc822-header->list port))
         (cdispo (assoc "content-disposition" headers))
         (ctype (assoc "content-type" headers)))
    (if (not (cdispo=form-data? (cadr cdispo)))
      #f
      (list (cdispo-name (cadr cdispo))
            (cdispo-filename (cadr cdispo))
            (if ctype (cadr ctype) #f)))))





; Returns 2 values: port filename.  The port will yield the data from the
; underlying port.  filename will be #f if the underlying port's data was <
; max-mem, otherwise it will be a path to the temporary file used to back this
; port's data.
;
; Rational: We may not know how much data is on an input port, for instance,
; when reading from a TCP port.  We may want to avoid reading an unknown amount
; of data into memory.  This custom port allows us to read all the data from
; the streaming port, and work with the created custom port as if we had all
; the data stored in memory.  If the amount of data is actually too large, a
; backing tempfile will hold the data.
;
; Reading all the data from the stream allows clients to access the data out of
; order.  For example, clients can access form-data field values in any order,
; and not have to worry about the fact that the data must actually be read and
; processed from the browser in the order received.
(define (make-stream-repeater-input-port port max-mem max-file)
  (let ((chunk-size 4096)
        (bytes-read 0)
        (buf "")
        (fd #f)
        (fname #f)
        (backing-port #f))

    ; Read all data from underlying port.
    (let loop ()
      (let* ((cur-chunk (read-string chunk-size port))
             (cur-chunk-size (if (eof-object? cur-chunk)
                               0
                               (string-length cur-chunk)))
             (done-reading?  (= 0 cur-chunk-size)))
        (set! bytes-read (+ bytes-read cur-chunk-size))
        (unless done-reading?
          (cond ((<= bytes-read max-mem)
                 (set! buf (string-append/shared buf cur-chunk)))
                ((<= bytes-read max-file)
                 (unless fd
                   ; Setup file backing, writing what we've read so far to the
                   ; file to start it off.
                   (set! fname (create-temporary-file))
                   (set! fd (file-open fname (+ open/rdwr open/creat)))
                   (file-write fd buf))
                 (file-write fd cur-chunk))
                (else
                  (error 'make-stream-repeater-input-port
                         (conc "max-file size of "
                               max-file
                               " reached for file "
                               fname))))
          (loop))))


    (if fd
      (begin
        ; Prepare file to be read from beginning
        (set-file-position! fd  0)
        (set! backing-port (open-input-file* fd)))
      (with-input-from-string buf (lambda()
                                    (set! backing-port (current-input-port)))))

    (values (make-input-port (lambda () (read-char backing-port))
                             (lambda () (char-ready? backing-port))
                             (lambda () (close-input-port backing-port)))
            fname)))

)  ; end module
