(use test multipart-form-data)


;;; Some helper functions
;;; =====================
(define (lf->crlf str)
  (string-translate* str '(("\n" . "\r\n"))))
(define wifs with-input-from-string)
(define cin current-input-port)
(define (with-field str proc)
  (wifs str (lambda ()
    (let ((f (mime-part->form-datum (cin))))
      (proc f)))))
(define (with-repeater str max-mem max-file proc)
  (wifs str (lambda ()
    (define-values (port fname) (make-stream-repeater-input-port (cin) max-mem max-file))
    (proc port fname))))




(test-group "header-parsing"
  (define cdispo "form-data; name=\"file1\"; filename=\"antigravity.py\"")
  (test #t (cdispo=form-data? cdispo))
  (test #f (cdispo=form-data? "foobar"))
  (test "file1" (cdispo-name cdispo))
  (test "antigravity.py" (cdispo-filename cdispo)))




(test-group "stream-repeater"
  (with-repeater "foobar" 6 8 (lambda (p f)
    (test-assert "no file needed" (not f))))
  (with-repeater "foobar" 3 8 (lambda (p f)
    (test-assert "file needed" f)
    (test-assert "file used"
          (equal? (read-string #f p)
                  (with-input-from-file f read-string)))))
  (test-error "max-file err"
              (with-repeater "foobar" 3 4 (lambda (p) #f)))
  (let ((str (make-string 4097 #\A)))
    (with-repeater str 4096 9999 (lambda (p f)
      (test-assert "file needed" f)
      (test "contents read" (string-length str)
                            (string-length (read-string #f p)))))))





(define text-input (lf->crlf #<<END
Content-Disposition: form-data; name="text-input"

foobar
END
))

(define file-upload (lf->crlf #<<END
Content-Disposition: form-data; name="file-upload"; filename="README"
Content-Type: application/octet-stream

Iterating over the mime-parts of a MIME multipart message without reading the
entire message into memory.

END
))

(define empty-file-upload (lf->crlf #<<END
Content-Disposition: form-data; name="empty-file-upload"; filename=""
Content-Type: application/octet-stream


END
))


(test-group "non-file-upload" (with-field text-input (lambda (f)
  (test "name" "text-input" (form-datum-name f))
  (test "filename" #f (form-datum-filename f))
  (test "ctype" #f (form-datum-ctype f))
  (test "value" "foobar" (read-string #f (form-datum-port f)))
  (test-assert (not (form-datum-tempfile f))))))

(test-group "file-upload" (with-field file-upload (lambda (f)
  (test "name" "file-upload" (form-datum-name f))
  (test "filename" "README" (form-datum-filename f))
  (test "ctype" "application/octet-stream" (form-datum-ctype f))
  (test "value"
        (conc "Iterating over the mime-parts"
              " of a MIME multipart message without"
              " reading the\r\n"
              "entire message into memory.\r\n")
        (read-string #f (form-datum-port f)))
  (test-assert (not (form-datum-tempfile f))))))

(test-group "empty-file-upload" (with-field empty-file-upload (lambda (f)
  (test "name" "empty-file-upload" (form-datum-name f))
  (test "filename" "" (form-datum-filename f))
  (test "ctype" "application/octet-stream" (form-datum-ctype f))
  (test "value" "" (read-string #f (form-datum-port f)))
  (test-assert (not (form-datum-tempfile f))))))


(test-exit)
